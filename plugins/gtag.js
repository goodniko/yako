import Vue from 'vue';
import VueGtag from 'vue-gtag';

export default ({ app, $config: { googleAnalyticsId } }) => {
  Vue.use(VueGtag, {
    config: { 
      id: googleAnalyticsId
    },
    appName: 'Yako',
  }, app.router);
}