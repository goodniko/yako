FROM node:14-alpine

# define arguments
ARG BASE_URL
ARG GOOGLE_ANALYTICS_ID
ARG EMAIL_TO
ARG EMAIL_FROM
ARG EMAIL_USER
ARG EMAIL_PASS

# create destination directory
RUN mkdir -p /app
WORKDIR /app

# update and install dependency
RUN apk update && apk upgrade
RUN apk add git

# copy the app, note .dockerignore
COPY . /app/
RUN npm install

# build necessary, even if no static files are needed,
# since it builds the server as well
RUN npm run build

# expose 5000 on container
EXPOSE 5000

# set app serving to permissive / assigned
ENV NUXT_HOST=0.0.0.0
# set app port
ENV NUXT_PORT=5000
# set base url
ENV BASE_URL=${BASE_URL}
# set gtag
ENV GOOGLE_ANALYTICS_ID=${GOOGLE_ANALYTICS_ID}
# set email
ENV EMAIL_TO=${EMAIL_TO}
ENV EMAIL_FROM=${EMAIL_FROM}
ENV EMAIL_USER=${EMAIL_USER}
ENV EMAIL_PASS=${EMAIL_PASS}

# start the app
CMD [ "npm", "start" ]