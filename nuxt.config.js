import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - Yako',
    title: 'Yako',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: "og:site_name", content: "Yako" },
      { name: "og:type", content: "profile" },
      { name: "og:image", content: "/avatar.jpg" },
      { name: "og:image:type", content: "image/jpg" },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  publicRuntimeConfig: {
    googleAnalyticsId: process.env.GOOGLE_ANALYTICS_ID || 'G-XXX',
  },

  // Route module configuration: https://github.com/vuejs/vuepress/blob/38e98634af117f83b6a32c8ff42488d91b66f663/packages/%40vuepress/plugin-active-header-links/clientRootMixin.js
  router: {
    // mode: 'history',
    scrollBehavior (to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition
      } else if (to.hash) {
        if (to.params.scrollOff){
          return false
        } else {
          const element = document.querySelector(decodeURIComponent(to.hash))
          if (element) {
            return window.scrollTo({ top: element.offsetTop, behavior: 'smooth' })
          } else {
            return { selector: decodeURIComponent(to.hash) }
          }
        }
      } else {
        // return { x: 0, y: 0 }
        return window.scrollTo({ top: 0, behavior: 'smooth' })
      }
    }
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/gtag',
    { 
      src: '@/plugins/scroll-progress',
      mode: 'client' 
    }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    '@nuxtjs/i18n',
    '@nuxtjs/axios',
    'nuxt-mail'
  ],

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  i18n: {
    locales: [ 
      { code: 'en', file: 'en.json' }, 
      { code: 'ru', file: 'ru.json' }],
    defaultLocale: 'en',
    lazy: true,
    langDir: 'locales',
  },

  axios: {
    baseURL: process.env.BASE_URL || 'http://localhost:3000',
    proxyHeaders: false,
    credentials: false
  },

  mail: {
    message: {
      to: process.env.EMAIL_TO || 'to-name@domain.com',
      from: process.env.EMAIL_FROM || 'from-name@domain.com'
    },
    smtp: {
      host: 'smtp-mail.outlook.com',
      tls: {
        ciphers: 'SSLv3'
      },
      port: 587,
      auth: {
        user: process.env.EMAIL_USER || 'user',
        pass: process.env.EMAIL_PASS || 'pass',
      },
    },
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      options: {
        customProperties: true
      },
      themes: {
        dark: {
          primary: colors.teal.lighten1,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: colors.teal.lighten1,
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
